console.log("HELLO KENNETH!!");



function countUsingWhile() {
	let input1 = document.getElementById('task1').value;
	
	if (input1 <= 0) {
		let msg = document.getElementById('message');
		msg.innerHTML = 'Value not valid';
	} else {
		while (input1 !== 0) {
		// what will happen if the condition has not/been met.
	alert(input1);
	input1--

		}
  	}
}


// let's create a function that will count to a series of number depending on the value inserted by the user

function countUsingDoWhile() {
	// get the input of the user.
	let number = document.getElementById('task2').value;

	if (number <= 0) {
		// the value is not valid.
		// inform the user that he/she cannot proceed.
		// using DOM Selectors we will target the container.
		let displayText = document.getElementById('info');
		displayText.innerHTML = 'The number is not valid';

	} else {
		// Proceed because the value is valid.

		let indexStart = 1;

		do {
			let displayText = document.getElementById('info');
			displayText.innerHTML = number + ' is valid';
			alert('count value: ' + indexStart);
			indexStart++
		} while (indexStart <= number);
	}
}

// for loop

function countUsingForLoop() {
	let data = document.getElementById('task3').value;
	
	let res = document.getElementById('response');

	if (data <= 0) {
		res.innerHTML = 'Input Invalid';
	} else {
		for (let startCount = 0; startCount <= data; startCount++) {
		
		alert(startCount);
		}
	}
}

//[SECTION] How to access elements of a string using repetition control structures? 




function accessElementsInString() {
   //get the input of the user. using DOM selectors
   let name = document.getElementById('userName').value;

   let textLength = document.getElementById('stringLength');
   // alert(typeof name); this is just a checker

   //validate and make sure that input is NOT equivalent to blank. 
   if (name !== '') {
     //response if truthy (valid)
     
      //analyze the value that was inserted by the user.
         //1. determine the length of the string. 
            //=> invoke the "length" property of a string using (.) notation. 
      textLength.innerHTML = 'The string is ' + name.length + ' characters long'; 
      
      //Upon accessing elements inside a string, this can be done so, using [] square brackets. 
      //keep in mind we can access each element through the use of it's index number/count
      //the count will start from 0 (first character inside the string corresponds to the number 0), the next is 1 and up to the "nth" number
	      // console.log(name[0]);
	      // console.log(name[1]);
	      // console.log(name[2]);
	      // console.log(name[3]);
	      // console.log(name[4]);
	      // console.log(name[5]);       
	      // console.log(name[6]);
	  //we will now use the concept of loops in order to produce a much more flexible response for the user's input
	      //initialization ; condition ; iteration
	  for (let startIndex = 0; startIndex < name.length; startIndex++) {
	  	 //access each element and display it inside the console
	  	 alert(name[startIndex]); 
	  }
   } else {
      //response if falsy 
      alert('value is Invalid'); 
   }  
}



// How to access elements of string? [value/index]


// Detect if the word is a palindrome.

// Behavior: if the string provided is an odd number, the middle character does not need to be checked.

// d a d // palindrome
// k a y a k //palindrome

// we will create a loop through half of the string characters that checks if the letters at the front and back of the string are the same.

function detectPalindrome() {
	// 1. get input using DOM Selectors
	let word = document.getElementById('word').value;

	let response = document.getElementById('detectPalindrome');
	
	// Validate the data if it is indeed the correct type of info.
	if (word !== '') {
		// Identify how long the word is.
		let wrdLength = word.length;
		console.log(wrdLength);
		// initialization => identify the starting point of the loop.
		// condition => describes how the loop will progress and how it will be terminated.
		// iteration => identify procedure on how to advance to the next loop.
		for (let index = 0; index < wrdLength / 2; index++) {
			// Instruction that will happen upon each iteration of the loop.
			// we are trying to get the current element in the string according to the index count.
			// get the last element of the string, we use -1. - deducting 1 in the current length of the string since the index count of each element inside a string starts with 0.
			if (word[index] !== word[wrdLength - 1 - index]) {
				// response truthy
				response.innerHTML = word + '<h3 class="text-primary"> is not a Palindrome</h3>';
			} else {
				console.log(word[index] + ' is the same as ' + word[wrdLength - 1 - index]);
				response.innerHTML = word + '<h3 class="text-success"> is a Palindrome</h3>'
			}
		}
		
	} else {
		// response falsy
		response.innerHTML = '<h3 class="text-danger">Value is invalid</h3>';
	}
}



// Create a function that will allow us to only display the odd number from a series of integers.

function getOddNumbers() {
	// target the value of the input field.
	let inputCount = document.getElementById('value4').value;

	let res = document.getElementById('getOddNum');

	// alert(inputCount);
	// Validation - validate the data to make sure to only get positive numbers
	if (inputCount > 0) {
		// response when pass

		for (let count = 0; count <= inputCount; count++) {
		// let's create another logic.
		if (count % 2 === 0) {
		// even number
			continue;
		}
		console.log(count);
	}

		// res.innerHTML = '<h3 class="text-warning"> Proceed</h3>'
	} else {
		// response when fail
		res.innerHTML = '<h3 class="text-danger"> The number should be greater than zero </h3>'
	}


}



let num = 0;
while (num < 5) {
    num++;
}
console.log(num);

let initialString = "JavaScript";
let newString = "";
for (let i=0; i<initialString.length; i+=2) {
    var ch = initialString[i];
    newString += ch;
}
console.log(newString);