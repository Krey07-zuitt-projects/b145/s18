console.log("Hello!!!");


function openClass1() {	
	
	let number = 50;

	for (let count = 0; count <= number; count++) {
		
		if (count % 10 === 0 || count % 5 === 0) {
			continue;
		}
		console.log(count);
	}
}

openClass1();



function stringWithoutVowels() {

	let initialString = 'Javascript';

	let resultWithoutVowels = '';

	for (let index = 0; index < initialString.length; index++) {
		let char = initialString[index];
		let NewChar = char.toLowerCase();

		if (NewChar === 'a' || NewChar === 'e' || NewChar === 'i' || NewChar === 'o' || NewChar === 'u') {
			continue;
		} else {
			resultWithoutVowels += NewChar;
		}
		console.log(resultWithoutVowels);
	}
}

stringWithoutVowels()